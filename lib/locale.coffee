_ = require 'lodash'

class Locale
    _translations: null
    constructor: (values) ->
        root = {}
        _add = (dir, values) ->
            for own k, v of values
                if typeof v is 'string'
                    root[dir + k] = _.template v
                else
                    _add(dir + k + '.', v)
        _add '', values
        @_translations = root
    get: (key, values...) ->
        if key of @_translations
            @_translations[key](values...)
        else
            key

module.exports = Locale
