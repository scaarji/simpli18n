fs = require 'fs'
path = require 'path'

_ = require 'lodash'

Locale = require './locale'

class Simpli18n
    constructor: (config) ->
        defaults =
            directory: __dirname + '/locales'
            default: null
            watch: no
        @_locales = {}

        @_config = _.extend defaults, config

        for file in fs.readdirSync(@_config.directory)
            filename = path.join @_config.directory, file
            continue unless fs.statSync(filename).isFile()

            locale = path.basename file, path.extname(file)

            do (locale, filename) =>
                @_add locale, filename
                if @_config.watch
                    fs.watch filename, =>
                        @_add locale, filename

    _add: (locale, filename) ->
        code = fs.readFileSync(filename, 'utf8').toString()

        try
            if filename.match /\.ya?ml$/
                yaml = require(['yaml', 'js'].join('-'))
                obj = yaml.load(code)
                obj = obj.shift() if obj.shift
            else if filename.match /\.json/
                obj = JSON.parse code
            else if filename.match /\.coffee/
                obj = coffee.eval code
            else
                console.log "Unsupported extension of locale file #{filename}"
        catch err
            console.log('Parsing file ' + filename, err, err.stack)

        if obj
            @_locales[locale] = new Locale obj

    getLocale: (locale = null) ->
        locale ?= @_options.default
        unless locale and locale of @_locales
            throw new Error "Locale #{locale} not defined"

        @_locales[locale]

module.exports = Simpli18n
